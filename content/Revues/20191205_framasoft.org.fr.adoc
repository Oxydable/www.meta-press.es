= [Framasoft.org] Une extension qui meta-press.es &agrave; ta disposition
:slug: framasoft_org_meta-press.es_a_ta_disposition
:lang: fr
:date: 2019-12-05
:author: Siltaar

https://framablog.org/2019/12/05/une-extension-qui-meta-press-es-a-ta-disposition/[*Une extension qui meta-press.es à ta disposition*]

La veille sur la presse en ligne est laborieuse et exigeante, mais une
extension pour Firefox peut la rendre plus légère, rapide et efficace… et plus
éthique que Google News.

Nous rencontrons aujourd’hui Simon Descarpentries pour lui poser des questions
sur le module Meta-Press.es qu’il a créé. […]

https://framablog.org/2019/12/05/une-extension-qui-meta-press-es-a-ta-disposition/
