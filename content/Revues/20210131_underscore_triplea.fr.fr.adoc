= [Underscore] &Eacute;mission Underscore #214 du 31 janvier 2021
:slug: 20210131_underscore_214
:lang: fr
:date: 2021-01-31
:author: Siltaar

== Actu

[…]

== Sujet : Meta-Press.es (interview https://mamot.fr/@Siltaer[Siltaer])

=== Comment te présente-t-on ?

Artisan du web (à mon compte via Acoeuro.com), ingénieur en informatique, engagé dans des associations (April, FDN, LQDN…).

Aujourd’hui, plus localement sur Bressuire, je m’investi dans le LUG de la ville GEBULL (qui couvre toute la moitier nord du département) et j’y répare entre autre bénévolement des Fairphones…

=== Comment t’es venue l’idée de Meta-Press.es ?

Justement en travaillant sur la revue de presse de la Quadrature du Net (pendant 5 ans). Il y avait beaucoup de travail et un outil sur mesure m’aurait permis d’être plus efficace et d’économiser du sommeil. Meta-Press.es c’est cet outil, il permet d’économiser 80% du temps qu’il me fallait pour réaliser la revue de presse en s’affranchissant de la plupart des copier/coller.

Mais Meta-Press.es, c’est aussi l’outil dont j’ai rêvé à l’époque pour remettre de la cohérence dans ma tâche. En effet, jusque là la revue de presse de la Quadrature s’appuyait surtout sur Google Actualités, alors que Google c’est le fer de lance des GAFAM, et donc une entreprise qui se retrouve souvent sous le feu des critiques de la Quadrature du Net. C’était assez inconfortable de dépendre d’un outil fourni par un ennemi.

C’est l’occasion de dire que la revue de presse c’est une action assez méconnue mais essentielle dans le fonctionnement de LQDN, qui permet notamment de monter en compétence sur des sujets puisqu’on est obligé du coup de lire tous les articles qui en parlent et s’en imprégner. (Coucou quota, coucou G0f !) C’est donc un bon point de départ pour contribuer à La Quadrature !

=== Et ça sert à quoi ? À qui s’adresse cet outil ?

Alors Meta-Press.es, ça sert à effectuer des recherches dans plusieurs journaux en même temps. Des centaines de journaux, qui permettrent de récupérer des milliers de résultats. Meta-Press.es les trie par ordre chronologique et on retrouve rapidement ce qui fait l’actualité d’un sujet.

Meta-Press.es va vite, protège la vie privée de ses utilisateurs (en évitant de charger les trackers publicitaires des journaux) et permet de s’affranchir de tout intermédiaire entre votre ordinateur et les journaux consultés. Rien ne passe par les serveurs de Meta-Press.es avec la promesse qu’on y touche pas, car il n’y a pas de serveurs pour Meta-Press.es. C’est bon pour le climat, exit les datacenters au cercle polaire… L’outil est véritablement décentralisé, c’est votre ordinateur qui bosse.

Meta-Press.es s’adresse à tous ceux qui utilisent Google Actualités, ou qui se retenaient de l’utiliser parce que c’était un outil Google. À toutes les associations qui ont une revue de presse à faire, à tous les journalistes qui font de la veille sur la presse… À tous ceux qui veulent approfondir un sujet d’actualité et croiser les sources. Ou encore à ceux qui contribuent à Wikipedia et cherchent plus de sources pour un article. Les usages sont nombreux.

Après, l’outil s’adresse à ceux qui utilisent Firefox, puisqu’il s’agit d’une extension du navigateur web Firefox. On l’installe depuis le magasin d’extensions de Mozilla, ça ajoute une icône à la barre de tâche et on ouvre le moteur de recherche en cliquant sur cette icône.

Au delà des simples recherches, il est possible d’exporter et importer des résultats (tous où une sélection seulement), de les filtrer par date ou par mots-clés, de choisir finement de quels sources chercher (il y a des journaux, des photos de la presse scientifique), et on pourra même prochainement programmer une recherche pour qu’elle se lance toute seule !

=== En quoi est-il différent d’outils comme Flus ou des aggrégateurs RSS ?

(On rappelle qu’on a fait une émission au sujet des flux RSS)

Un agrégateur de flux RSS permet de s’abonner à des “sites webs” pour aller en chercher le nouveau contenu régulièrement. Il conserve ces contenus et permet de parcourir l’historique.
Meta-Press.es par contre, va chercher du contenu (éventuellement ancien) à partir des moteurs de recherche des journaux référencés.

Une fois la recherche terminée, quand on lance une nouvelle recherche, les précédents résultats récupérés sont évacués.

La confusion pourrait venir du fait que Meta-Press.es utilise le format RSS pour exporter les résultats d’une recherche, et donc les ré-importer.

Et même encore mieux, Meta-Press.es permet de sélectionner des résultats, pour n’exporter que les résultats voulus.

=== D’ailleurs, techniquement, on utilise aussi les flux RSS ? Y en a pas toujours à disposition, si ?

Parmis les 250 sources dans lesquelles Meta-Press.es sait chercher, un quart proposer un flux RSS des résultats d’une recherche. On utilise alors prioritairement ces flux, car leur mise en forme est standard et constante. Pour les autres sources, il faut analyser l’interface web de la page des résultats, et cette interface est susceptible de changer au gré des caprices du département marketing…

Donc non, il n’y a pas toujours un flux de dispo, et c’est bien dommage, car ça nous simplifie bien la vie quand il y en a un.

=== Comment peut-on t’aider ? On peut donner des pépètes ?

Le meilleur moyen de m’aider, c’est de contribuer, par exemple en ajoutant de nouvelles sources à l’outil.

Après, si on est pas développeur web, on peut quand même aider à faire connaître l’outil.

Et enfin, si c’est plus facile, on peut aussi donner des sous (via https://patreon.com/metapress[Patreon] ou Liberapay) pour que je puisse avec le temps de faire tout ça moi.

=== Tu as d’autres projets il me semble, et tu contribues aussi à d’autres logiciels libres ?

Il y a des petits projets issus de Meta-Press.es, qui bénéficient de leur propres dépôt de code sur Framagit. Il s’agit d’outils réutilisables, écrits en JavaScript :

 - https://framagit.org/Siltaar/month_nb[month_nb] : permet retrouver le numéro d’un mois dont on connait le nom, en toutes lettres, quelque soit sa langue ;
 - https://framagit.org/Siltaar/gettext_html_auto.js[gettext_html_auto] : permet de trouver et de remplacer toutes les chaînes de caractère d’une page web (pour gérer les traductions d’une extension web, à partir d’une structure JSON à GNU Gettext, plus simple que celle prévue pour les WebExtensions).

J’écris également un blog un peu technique pour partager mes découvertes en ligne de commande GNU+Linux, il s’appelle https://www.grimoire-command.es[Grimoire-Command.es].

Enfin je n’hésite jamais à dégainer un rapport de bug ou une demande de fonctionnalité (et ça prends du temps !)

https://www.triplea.fr/blog/podcast/emission-underscore-214-du-31-janvier-2021/[*Émission Underscore #214 du 31 janvier 2021*]

*Bonus : Astrologeek*

[…]

*libriste* : Meta-Press.es … que réussi à m’avoir !

[…]
