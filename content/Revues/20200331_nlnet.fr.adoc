= [NLnet.nl] Meta-Press.es pour s'&eacute;chapper du bourbier du pistage publicitaire en ligne
:slug: 20200331_nlnet
:lang: fr
:date: 2020-03-31
:author: Siltaar

La fondation NLnet.nl a ajouté Meta-Press.es à sa https://nlnet.nl/project/current.html[liste des projets actuellement soutenus], et en a fait une https://nlnet.nl/project/Meta-Presses/[bonne présentation].

[source,text,subs="verbatim,quotes"]
----
Meta-Press.es est un moteur de recherche pour la presse, sous la forme d'un module de navigateur web. Lorsqu'on l'utilise, tout se passe entre l'ordinateur de l'utilisateur et les journaux interrogés. Avec Meta-Press.es, aucune information n'est envoyée aux pisteurs en ligne (ce qui inclut les serveurs de Meta-Press.es ou de la NLnet.nl). Nous ne vous demandons de simplement nous croire quant au restpet de votre vie privée, avec ce logiciel (libre), il s'agit d'un fait vérifiable. Cela signifie également qu'il n'y a pas un unique point faible dans le système par lequel toutes les requêtes transitent, et qu'il est facile de surveiller, de contrôler voire d'arrêter.

*En quoi est-ce important pour les utilisateur ?*

Pour s'informer en ligne, il faut affronter sur le site de chaque journal en ligne un véritable bourbier de pistage (principalement publicitaire, au plus offrant) ou alors s'appuyer sur des agrégateurs qui offrent peu de contrôle sur ce qu'ils affichent et vendent les places de leur sélection de sujets tendances… Votre moteur de recherche actuel glisse peut être même une ou deux intox au milieu des résultats pertinents, juste parce que ces mensonges sont sensationnels et attirent beaucoup d'attention (injustifiée).

Au lieu de lutter continuellement pour s'évader tant bien que mal de ces pièges de surveillance et d'usurpation, les lecteurs de journaux cherchent simplement à accéder aux contenus fiables des sources qu'ils choisissent pour se tenir à jour. Ce projet développe un moteur de recherche permettant cela, sous la forme d'un module de Firefox, avec lequel tout se passe strictement entre vous et les journaux qui vous intéressent. Pas de surveillance, pas de censure ni de contenus tiers, malhonnêtes mais sponsorisés. À la place, une recherche directe et fiable.
----
