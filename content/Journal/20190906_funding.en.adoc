= Funds from the Wau Holland Foundation
:slug: funds-from-the-wau-holland-foundation
:lang: en
:date: 2019-09-06
:author: Siltaar

Since the last news, and after 6 months of work on the proof of concept, I did
seek some funds.

I applied to :

- the Awesome foundation
- the Charles Léopold Mayer foundation
- la Francophonie
- Mozilla via the MOSS
- the NLnet foundation
- the Open Society Institute (OSI)
- the OpenTech Fund (OTF)
- the Shuttleworth Foundation
- the Wau Holland Foundation

And tried to apply to an European funding, which turned out to require
connections in 2 other European countries.

I also could have asked the Fund for Defense of Net Neutrality
https://www.fdn2.org[FDN²], but as I'm the current treasurer of the structure,
it would require extra care and still cast a shadow in some minds.

I had a answers from nearly all of the foundations.

Mozilla, NLnet and the OSI answered : "Maybe, but it's a bit too early". I can
understand their point of view, but once the work will be done, it might be a
bit too late.

The https://www.wauland.de/en/[Wauland] foundation finally issued a positive
answer in novembre 2018 for 5k€.

I had too much contracts to work on to resume working on Meta-Press.es earlier
this year, but a coming event (that will soon be announced here) will profit
from the last enhancements of the project.

So far, a lot of work is still needed before submitting the addon to
https://addons.mozilla.org/[AMO], but a selection mechanism of the newspapers to
search in is now working, and 42 newspapers are indexed. I will write an How-To
index one more newspaper, but the current official method is to get the
inspiration in https://framagit.org/Siltaar/meta-press-ext/blob/master/js/newspapers.js[`js/newspapers.js`]. Less than 20 lines of JavaScript are required
to parse the results of a newspaper.

There is still some small bugs to fix, and mainly the import/export of results
(or selections) in JSON, RSS and ATOM formats to code. But starting from now,
with the filter mechanism working, the project can already integrate newspapers
scraping contributions (or tagging enhancements of current newspapers).
