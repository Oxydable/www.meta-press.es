= Join Meta-Press.es for one year of apprentissage ?
:slug: un_an_apprentissage_2020
:lang: en
:date: 2020-08-11
:author: Siltaar

Meta-Press.es development is going on, despite a long summer break.

Great contributors added new sources since the last update and I'm working on a
thinner right management for the add-on.

But today I'm not speaking about technical stuff, it's about human. Today, as per new _apprentissage_ subvention by the French government, Acoeuro start recruiting an apprentice, to work one year in _alternance_ for Meta-Press.es

The ideal applicant :

- just got his Bac+2 diploma with web development in it ;
- is under 22 ;
- is capable of adding a new source to Meta-Press.es ;
- understands what's at stake and so is willing to add a lot of new sources ;
- wants to work more generally on this libre-software WebExtension with https://s.d12s.fr/CV-en.html[me] in Pougne-Hérisson (Deux-Sèvres, France).

A formal call have been issued here : https://alternance.emploi.gouv.fr/portail_alternance/jcms/gc_5454/bourse-a-l-emploi-recherche?jsp=plugins/BourseEmploiPlugin/jsp/recherche.jsp&a=ConsulterOffreEmploi&reference=PA208687

I will announce the result here.
