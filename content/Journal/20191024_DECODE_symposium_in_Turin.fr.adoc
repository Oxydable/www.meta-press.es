= Meta-Press.es au symopsium DECODE &agrave; Turin
:slug: decode_symposium_turin
:lang: fr
:date: 2019-10-24
:author: Siltaar

Le planning du symopsium DECODE est sorti, deux semaines avant l'évènement :

https://decodeproject.eu/events/workshops-agenda-5th-november

Je présenterai Meta-Press.es lors de la dernière séance du 5 novembre (vers 17h50).

Des démonstrations devraient également pouvoir se tenir à la table
Meta-Press.es dans le village des associations.
